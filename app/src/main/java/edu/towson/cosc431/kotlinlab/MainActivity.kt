package edu.towson.cosc431.kotlinlab

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        val PERMISSION_REQUEST_CODE = 42
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, arrayOf(
                    android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION), PERMISSION_REQUEST_CODE)


        } else {
            //permission was granted
            readLocation()
        }


    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode) {
            PERMISSION_REQUEST_CODE -> {
                if(permissions[0] == android.Manifest.permission.ACCESS_FINE_LOCATION) {
                    if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        readLocation()
                    } else {
                        //denied!
                        location.text = "Permission Denied"
                    }
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun readLocation() {

        try {

            val locManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val loc: Location? = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
            locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10.0f,
                    object : LocationListener {
                        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
                        }

                        override fun onProviderDisabled(provider: String?) {
                        }

                        override fun onProviderEnabled(provider: String?) {
                        }

                        override fun onLocationChanged(loc: Location?) {
                            location.text = loc?.latitude.toString() + ", " + loc?.longitude.toString()
                        }

                    })

        } catch (e: SecurityException) {
            location.text = "Oops. Permission denied"
        }
    }
}
